## Color scheme file for all python scripts ##

# colors.py

DoomOne = [
    ["#282c34", "#282c34"], # bg
    ["#bbc2cf", "#bbc2cf"], # fg
    ["#1c1f24", "#1c1f24"], # color01
    ["#ff6c6b", "#ff6c6b"], # color02
    ["#98be65", "#98be65"], # color03
    ["#da8548", "#da8548"], # color04
    ["#51afef", "#51afef"], # color05
    ["#c678dd", "#c678dd"], # color06
    ["#46d9ff", "#46d9ff"]  # color15
]

Dracula = [
    ["#282a36", "#282a36"], # bg
    ["#f8f8f2", "#f8f8f2"], # fg
    ["#44475a", "#44475a"], # color01
    ["#ff5555", "#ff5555"], # color02
    ["#50fa7b", "#50fa7b"], # color03
    ["#f1fa8c", "#f1fa8c"], # color04
    ["#bd93f9", "#bd93f9"], # color05
    ["#ff79c6", "#ff79c6"], # color06
    ["#8be9fd", "#8be9fd"]  # color07
]

Nord = [
    ["#2e3440", "#2e3440"], # bg
    ["#d8dee9", "#d8dee9"], # fg
    ["#3b4252", "#3b4252"], # color01
    ["#bf616a", "#bf616a"], # color02
    ["#a3be8c", "#a3be8c"], # color03
    ["#ebcb8b", "#ebcb8b"], # color04
    ["#81a1c1", "#81a1c1"], # color05
    ["#b48ead", "#b48ead"], # color06
    ["#88c0d0", "#88c0d0"]  # color07
]

OceanicNext = [
    ["#1B2B34", "#1B2B34"], # bg
    ["#D8DEE9", "#D8DEE9"], # fg
    ["#343D46", "#343D46"], # color01
    ["#EC5f67", "#EC5f67"], # color02
    ["#99C794", "#99C794"], # color03
    ["#FAC863", "#FAC863"], # color04
    ["#6699CC", "#6699CC"], # color05
    ["#C594C5", "#C594C5"], # color06
    ["#5FB3B3", "#5FB3B3"]  # color07
]

Palenight = [
    ["#292D3E", "#292D3E"], # bg
    ["#BFC7D5", "#BFC7D5"], # fg
    ["#434758", "#434758"], # color01
    ["#F07178", "#F07178"], # color02
    ["#C3E88D", "#C3E88D"], # color03
    ["#FFCB6B", "#FFCB6B"], # color04
    ["#82AAFF", "#82AAFF"], # color05
    ["#C792EA", "#C792EA"], # color06
    ["#89DDFF", "#89DDFF"]  # color07
]

SolarizedDark = [
    ["#002b36", "#002b36"], # bg
    ["#839496", "#839496"], # fg
    ["#073642", "#073642"], # color01
    ["#dc322f", "#dc322f"], # color02
    ["#859900", "#859900"], # color03
    ["#b58900", "#b58900"], # color04
    ["#268bd2", "#268bd2"], # color05
    ["#d33682", "#d33682"], # color06
    ["#2aa198", "#2aa198"]  # color07
]

RoseLinuxDark = [
    ["#121217", "#121217"], # bg
    ["#bbc8ca", "#bbc8ca"], # fg
    ["#373b41", "#373b41"], # color01
    ["#a8c97e", "#a8c97e"], # color02
    ["#cc6666", "#cc6666"], # color03
    ["#5fb3b3", "#5fb3b3"], # color04
    ["#c594c5", "#c594c5"], # color05
    ["#ff5c92", "#ff5c92"], # color06
    ["#e6c547", "#e6c547"]  # color07
]

