## Script used to recolor the alacritty terminal 

import os
import sys

import os
import colors

def generate_alacritty_config(color_scheme):
    # Apply the specified changes to the color scheme
    background = color_scheme[0][0]
    foreground = color_scheme[1][0]
    color01 = color_scheme[2][0]
    color02 = color_scheme[4][0]
    color03 = color_scheme[3][0]
    color04 = color_scheme[7][0]  
    color05 = color_scheme[5][0] 
    color06 = color_scheme[6][0]  
    color07 = color_scheme[8][0]  
    color08 = color_scheme[1][0]  

    config = f"""
# alacritty.toml

# Window settings
[window]
padding = {{ x = 10, y = 10 }}
decorations = "full"

# Font settings
[font]
normal = {{ family = "DejaVu Sans Mono" }}
size = 12.0

# Colors
[colors]
[colors.primary]
background = "{background}"
foreground = "{foreground}"

[colors.cursor]
text = "{background}"
cursor = "{foreground}"

[colors.selection]
text = "{background}"
background = "{foreground}"

[colors.normal]
black = "{color01}"
red = "{color02}"
green = "{color03}"
yellow = "{color04}"
blue = "{color05}"
magenta = "{color06}"
cyan = "{color07}"
white = "{color08}"

[colors.bright]
black = "{background}"
red = "{color02}"
green = "{color03}"
yellow = "{color04}"
blue = "{color05}"
magenta = "{color06}"
cyan = "{color07}"
white = "{foreground}"
"""
    return config

# Choose your color scheme
color_scheme = colors.RoseLinuxDark
config = generate_alacritty_config(color_scheme)

# Write to the Alacritty configuration file
config_path = os.path.expanduser("~/.config/alacritty/alacritty.toml")
with open(config_path, "w") as f:
    f.write(config)

print("Alacritty configuration generated successfully.")

