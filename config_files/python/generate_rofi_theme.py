## Script used to generate rofi themes from colors.py

import os
import sys

# Add the directory containing colors.py to the Python path
sys.path.append(os.path.expanduser("~/.config/python"))

# Now import the colors module
import colors

def generate_rofi_theme(color_scheme):
    rofi_theme = f"""
* {{
    background: "{color_scheme[0][0]}";
    foreground: "{color_scheme[1][0]}";
    border-color: "{color_scheme[2][0]}";
    separatorcolor: "{color_scheme[2][0]}";
    selected-normal-background: "{color_scheme[4][0]}";
    selected-normal-foreground: "{color_scheme[0][0]}";
    urgent-background: "{color_scheme[3][0]}";
    urgent-foreground: "{color_scheme[1][0]}";
    active-background: "{color_scheme[5][0]}";
    active-foreground: "{color_scheme[1][0]}";
}}

window {{
    location: "east";
    anchor: "east";
    fullscreen: false;
    width: 30%;
    padding: 10px;
    border: 2px solid {color_scheme[2][0]};
}}

listview {{
    fixed-height: 0;
    lines: 10;
    padding: 5px;
    border: 1px solid {color_scheme[2][0]};
    separator: "dash";
}}

element {{
    padding: 5px;
}}

element.normal.normal {{
    background-color: "{color_scheme[0][0]}";
    text-color: "{color_scheme[1][0]}";
}}

element.selected.normal {{
    background-color: "{color_scheme[4][0]}";
    text-color: "{color_scheme[0][0]}";
}}

element.urgent.normal {{
    background-color: "{color_scheme[3][0]}";
    text-color: "{color_scheme[1][0]}";
}}

element.active.normal {{
    background-color: "{color_scheme[5][0]}";
    text-color: "{color_scheme[1][0]}";
}}

inputbar {{
    children: [ "prompt", "entry", "case-indicator" ];
    padding: 5px;
    spacing: 5px;
    border: 1px solid {color_scheme[2][0]};
}}

prompt {{
    text-color: "{color_scheme[1][0]}";
}}

entry {{
    text-color: "{color_scheme[1][0]}";
}}

case-indicator {{
    text-color: "{color_scheme[1][0]}";
}}
"""
    return rofi_theme

# Choose your color scheme
color_scheme = colors.RoseLinuxDark
rofi_theme = generate_rofi_theme(color_scheme)

# Write to the Rofi theme file
rofi_theme_path = os.path.expanduser("~/.config/rofi/config.rasi")
os.makedirs(os.path.dirname(rofi_theme_path), exist_ok=True)
with open(rofi_theme_path, "w", encoding="utf-8") as f:
    f.write(rofi_theme)

print("Rofi theme generated successfully.")


